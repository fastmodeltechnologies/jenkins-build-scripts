# Setup

#### Instructions

- Install standalone Groovy
    - Personally, I'm a big fan of [SDKMan](https://sdkman.io/)
- Add Groovy to PATH:

      export GROOVY_HOME={path_to_groovy_folder}
      export PATH=$PATH:$GROOVY_HOME/bin
- WIP...
